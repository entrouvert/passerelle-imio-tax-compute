# passerelle-imio-tax-compute - passerelle connector to Lisrue webservice
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from urllib.parse import urlparse

import requests
from django.db import models
from django.utils.translation import gettext_lazy as _
from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint


class ImioTsTaxCompute(BaseResource):
    formule = models.TextField(verbose_name=_('Python code to compute taxation result'))
    category = _('Compute Module')

    class Meta:
        verbose_name = _('Tax compute Service')

    @classmethod
    def get_verbose_name(cls):
        return cls._meta.verbose_name

    @classmethod
    def get_icon_class(cls):
        return 'gis'

    @classmethod
    def get_connector_slug(cls):
        return 'imio-tax-compute'

    @endpoint(methods=['post'])
    def eval(self, request, *args, **kwargs):
        data = {x: request.GET[x] for x in request.GET.keys()}
        if request.body:
            payload = json.loads(request.body)
            data.update(payload.get('fields'))
        expression = self.formule
        code_object = compile(expression, '<string>', 'exec')
        eval(code_object, data)
        result = data.get('result')
        return {'data': result}
