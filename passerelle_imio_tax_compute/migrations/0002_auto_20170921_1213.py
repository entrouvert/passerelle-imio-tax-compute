from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_imio_tax_compute', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imiotstaxcompute',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
